﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using System;

namespace BSA2021_ProjectStructure.PL.Services.Factories
{
    public class TeamFactory : IModelFactory<TeamModel, NewTeamModel>
    {
        public NewTeamModel CreateModel()
        {
            Console.Write("Name > ");
            string name = Console.ReadLine();

            return new NewTeamModel
            {
                Name = name
            };
        }

        public void UpdateModel(TeamModel entity)
        {
            Console.Write("Change name [y/n] > ");
            char key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("Name > ");
                string name = Console.ReadLine();
                entity.Name = name;
            }
        }
    }
}
