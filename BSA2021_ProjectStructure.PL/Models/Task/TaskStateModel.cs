﻿

namespace BSA2021_ProjectStructure.PL.Models
{
    public enum TaskStateModel: byte
    {
        Undefined = 0,
        Unfinished,
        Finished,
        Canceled
    }
}
