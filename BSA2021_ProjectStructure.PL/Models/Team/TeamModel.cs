﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class TeamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return $"ID: {Id}, Name: {Name}";
        }
    }
}
