﻿

namespace BSA2021_ProjectStructure.PL.Models.QueryResultModels
{
    public class ProjectDetailModel
    {
        public ProjectModel Project { get; set; }
        public TaskModel LongestDescriptionTask { get; set; }
        public TaskModel ShortestNameTask { get; set; }
        public int? TeamMembersCount { get; set; }
    }
}
