﻿using System.Collections.Generic;

namespace BSA2021_ProjectStructure.PL.Models.QueryResultModels
{
    public class PerformerWithTasksModel: UserModel
    {
        public List<TaskModel> Tasks { get; set; } = new List<TaskModel>();
    }
}
