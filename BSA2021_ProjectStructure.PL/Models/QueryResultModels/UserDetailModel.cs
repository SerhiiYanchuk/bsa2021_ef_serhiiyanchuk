﻿
namespace BSA2021_ProjectStructure.PL.Models.QueryResultModels
{
    public class UserDetailModel
    {
        public UserModel User { get; set; }
        public ProjectModel LastProject { get; set; }
        public int ProjectTasksCount { get; set; }
        public int UserUnfinishedTasksCount { get; set; }
        public TaskModel LongestTask { get; set; }
    }
}
