﻿

namespace BSA2021_ProjectStructure.Common.DTO
{
    public enum TaskStateDTO: byte
    {
        Undefined = 0,
        Unfinished,
        Finished,
        Canceled
    }
}
