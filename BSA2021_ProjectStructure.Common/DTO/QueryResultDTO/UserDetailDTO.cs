﻿
namespace BSA2021_ProjectStructure.Common.DTO.QueryResultDTO
{
    public class UserDetailDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int ProjectTasksCount { get; set; }
        public int UserUnfinishedTasksCount { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
