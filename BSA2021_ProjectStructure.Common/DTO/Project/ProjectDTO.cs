﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.Common.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }      
        public UserDTO Author { get; set; }
        public int TeamId { get; set; }
        public TeamDTO Team { get; set; }
        public List<TaskDTO> Tasks { get; set; } = new List<TaskDTO>();
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
