﻿using System.Collections.Generic;
using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public void LoadProjects(User user)
        {
            _context.Entry(user).Collection(p => p.Projects).Load();
        }
        public void LoadTasks(User user)
        {
            _context.Entry(user).Collection(p => p.Tasks).Load();
        }
    }
}
