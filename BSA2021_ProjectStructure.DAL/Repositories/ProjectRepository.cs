﻿using System.Collections.Generic;
using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public void LoadTasks(Project project)
        {
            _context.Entry(project).Collection(p => p.Tasks).Load();
        }
        public void LoadAuthor(Project project)
        {
            _context.Entry(project).Reference(p => p.Author).Load();
        }
        public void LoadTeam(Project project)
        {
            _context.Entry(project).Reference(p => p.Team).Load();
        }
    }
}
