﻿using System.Collections.Generic;
using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class TaskRepository : Repository<Task>, ITaskRepository
    {
        public TaskRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public void LoadProject(Task task)
        {
            _context.Entry(task).Reference(p => p.Project).Load();
        }
        public void LoadPerformer(Task task)
        {
            _context.Entry(task).Reference(p => p.Performer).Load();
        }
    }
}
