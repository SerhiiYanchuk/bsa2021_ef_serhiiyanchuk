﻿using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using BSA2021_ProjectStructure.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly ProjectDbContext _context;
        public Repository(ProjectDbContext context)
        {
            _context = context;
        }
        public virtual IEnumerable<TEntity> GetAll(bool isTracked = true)
        {
            IQueryable<TEntity> set = _context.Set<TEntity>();
            if (!isTracked)
                set.AsNoTracking();
            return set.ToList();
        }
        public virtual TEntity FindById(int id)
        {
            return _context.Find<TEntity>(id);
        }
        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, bool isTracked = true)
        {
            IQueryable<TEntity> set = _context.Set<TEntity>().Where(predicate);
            if (!isTracked)
                set.AsNoTracking();
            return set.ToList();
        }
        public virtual void Insert(TEntity item)
        {
            _context.Add(item);
        }
        public virtual void Update(TEntity item)
        {
            _context.Update(item);
        }
        public virtual void Delete(int id)
        {
            TEntity item = FindById(id);
            Delete(item);
        }
        public virtual void Delete(TEntity item)
        {
            if (_context.Entry(item).State == EntityState.Detached)
                _context.Attach(item);
            _context.Remove(item);
        }
        public virtual bool CheckAvailability(int id)
        {
            return _context.Set<TEntity>().Any(p => p.Id == id);
        }
        public virtual IQueryable<TEntity> GetQuery()
        {
            return _context.Set<TEntity>();
        }
    }
}
