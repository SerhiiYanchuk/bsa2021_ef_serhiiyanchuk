﻿using System.Collections.Generic;
using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public void LoadMembers(Team team)
        {
            _context.Entry(team).Collection(p => p.Members).Load();
        }
        public void LoadProjects(Team team)
        {
            _context.Entry(team).Collection(p => p.Projects).Load();
        }
    }
}
