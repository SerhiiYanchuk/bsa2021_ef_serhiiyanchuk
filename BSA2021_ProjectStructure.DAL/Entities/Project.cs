﻿using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.DAL.Entities
{
    public class Project: Entity
    {
        public int AuthorId { get; set; }      
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; } = new List<Task>();
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
