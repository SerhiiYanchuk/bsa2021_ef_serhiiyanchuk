﻿using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IEnumerable<TEntity> GetAll(bool isTracked = true);
        TEntity FindById(int id);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, bool isTracked = true);
        void Insert(TEntity item);
        void Update(TEntity item);
        void Delete(int id);
        void Delete(TEntity item);
        bool CheckAvailability(int id);
        IQueryable<TEntity> GetQuery();
    }
}
