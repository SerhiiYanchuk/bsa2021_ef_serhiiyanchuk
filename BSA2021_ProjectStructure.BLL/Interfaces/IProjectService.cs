﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetAll();
        ProjectDTO FindById(int id);
        ProjectDTO Insert(NewProjectDTO item);
        void Update(ProjectDTO item);
        void Delete(int id);
        void Delete(ProjectDTO item);
        bool CheckAvailability(int id);
    }
}
