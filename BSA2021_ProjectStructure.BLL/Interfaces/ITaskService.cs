﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetAll();
        TaskDTO FindById(int id);
        TaskDTO Insert(NewTaskDTO item);
        void Update(TaskDTO item);
        void Delete(int id);
        void Delete(TaskDTO item);
        bool CheckAvailability(int id);
    }
}
