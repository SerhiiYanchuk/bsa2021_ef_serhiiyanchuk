﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<ProjectDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_unitOfWork.Projects.GetAll(false));
        }
        public ProjectDTO FindById(int projectId)
        {
            return _mapper.Map<ProjectDTO>(_unitOfWork.Projects.FindById(projectId));
        }
        public ProjectDTO Insert(NewProjectDTO projectDTO)
        {
            var createdProject = _mapper.Map<Project>(projectDTO);
            _unitOfWork.Projects.Insert(createdProject);
            _unitOfWork.SaveChanges();
            return _mapper.Map<ProjectDTO>(createdProject);
        }

        public void Update(ProjectDTO projectDTO)
        {
            Project editProject = _unitOfWork.Projects.FindById(projectDTO.Id);
            _mapper.Map<ProjectDTO, Project>(projectDTO, editProject);
            _unitOfWork.Projects.Update(editProject);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int projectId)
        {
            _unitOfWork.Projects.Delete(projectId);
            _unitOfWork.SaveChanges();
        }

        public void Delete(ProjectDTO projectDTO)
        {
            _unitOfWork.Projects.Delete(_mapper.Map<Project>(projectDTO));
            _unitOfWork.SaveChanges();
        }
        public bool CheckAvailability(int projectId)
        {
            return _unitOfWork.Projects.CheckAvailability(projectId);
        }
       
    }
}
