﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA2021_TeamStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetTeams()
        {
            return Ok(_teamService.GetAll());
        }
        [HttpGet("info")]
        public ActionResult<IEnumerable<TeamShortInfoDTO>> GetTeamsShortInfo([FromServices] ILinqSelectionService linqSelectionService)
        {
            return Ok(linqSelectionService.GetTeamShortInfo());
        }
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetTeam(int id)
        {
            var team = _teamService.FindById(id);
            if (team is null)
                return NotFound("ID doesn't exist");
            return Ok(team);
        }

        [HttpPost]
        public ActionResult<TeamDTO> PostTeam([FromBody] NewTeamDTO teamDTO)
        {
            var createdTeam = _teamService.Insert(teamDTO);
            return Created("/api/teams/" + createdTeam.Id, createdTeam);
        }

        [HttpPut]
        public IActionResult PutTeam([FromBody] TeamDTO teamDTO)
        {
            if (!_teamService.CheckAvailability(teamDTO.Id))
                return NotFound("ID doesn't exist");

            _teamService.Update(teamDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTeam(int id)
        {
            if (!_teamService.CheckAvailability(id))
                return NotFound("ID doesn't exist");

            _teamService.Delete(id);
            return NoContent();
        }
    }
}
